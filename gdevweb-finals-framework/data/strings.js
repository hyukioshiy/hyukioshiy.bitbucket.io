strings = {
    "en": {
        "TEST": "Loading",
        "IS_MOBILE": "Testing on Mobile...",
        "IS_DESKTOP": "Testing on Desktop...",
        "ENDCARD_HEADER_WON": "YOU WON!",
        "ENDCARD_HEADER_LOST": "YOU LOST!",
        "ENDCARD_BODY": "Are you up\nto the challenge?",
        "INSTALL_NOW": "INSTALL NOW"
    },
    "fil": {
        "TEST": "Kinakarga",
        "IS_MOBILE": "Sinusubukan sa Cellphone...",
        "IS_DESKTOP": "Sinusubukan sa Kompyuter...",
        "ENDCARD_HEADER_WON": "NANALO KA!",
        "ENDCARD_HEADER_LOST": "NATALO KA!",
        "ENDCARD_BODY": "Kaya mo pa ba\nang ibang hamon?",
        "INSTALL_NOW": "I-INSTALL NGAYON"
    }
}