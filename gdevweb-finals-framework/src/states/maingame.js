class MainGame extends Phaser.State {
    

    preload() {
        this.game.load.image('santa', 'assets/santa.png');
        this.game.load.image('bg', 'assets/bggame.png');
        this.game.load.image('gift', 'assets/gifts.png');
        this.game.load.image('ui', 'assets/UI.png');
        this.game.load.image('install', 'assets/UI.png');
    }

    create() {
        
        game.physics.startSystem(Phaser.Physics.ARCADE);
        game.physics.arcade.gravity.y = 100;

        this.background = this.game.add.image(350, 400, 'bg');
        this.background.anchor.setTo (0.5, 0.5); 
        this.background.scale.setTo(.8,.8);

        this.santa = game.add.sprite(350,700, 'santa');
        game.physics.enable(this.santa, Phaser.Physics.ARCADE);
        this.santa.scale.setTo(.3,.3);
        
        this.ui = this.game.add.sprite(150,100,'ui');
        this.ui.anchor.setTo (0.5, 0.5); 
        this.ui.scale.setTo(.4,.4);

        this.counter = 30;

        this.text = game.add.text(170, 100, 'Time : 30', { font: "32px Arial", fill: "#000000", align: "center" });
        this.text.anchor.setTo(0.5, 0.5);
        
        this.scoretext = game.add.text(game.world.centerX- 30, game.world.centerY - 200, 'Gifts : 0', { font: "64px Arial", fill: "#000000", align: "center" });
        this.scoretext.anchor.setTo(0.5, 0.5);
        
        game.time.events.loop(Phaser.Timer.SECOND, this.updateCounter, this);

        this.timer = 0;
        this.total = 0;
        
        this.score = 0;
        
        this.curr = 0;
        this.next = 0;

        this.randInt = 0;
        this.spawned = false;

        this.gifts = [];

        this.spawnGift();
    }

    updateCounter() {

        if(this.counter > 0)
            this.counter--;
        this.text.setText('Time : ' + this.counter);
    
    }

    updateScore()
    {

    }
    update()
    {
        if(this.counter  == this.next)
        {
            this.spawned = false;
        }

        this.spawnGift();

        for (var i = this.gifts.length; i >= 0; i--) {
            game.physics.arcade.overlap(this.santa, this.gifts[i], this.overlapHandler, null, this);
        }        

        this.santa.x = game.input.x;
        this.santa.body.collideWorldBounds = true;

        if(this.counter < 0)
        {
            this.gameOver();
        }

    }

    overlapHandler(santa,gift)
    {
        gift.kill();
        this.score++;
        this.scoretext.setText('Gifts : ' + this.score);
        this.gifts.splice(0, this.gifts.indexOf(gift));
    }
    spawnGift()
    {
        if(this.counter % 3 == 0)
        {
            if(this.spawned == false)
            {
                this.curr = this.counter;
                this.next = this.curr - 3;
                
                this.randInt = game.rnd.integerInRange(0, 900);
                var gift = game.add.sprite(this.randInt,-100, 'gift');
                gift.scale.setTo(.2,.2);
                game.physics.enable(gift, Phaser.Physics.ARCADE);
                this.gifts.push (gift);
                this.spawned = true;
            }
        }
    }

    
    //Unused Functions
    gameStart () {
       
    }
    gameOver () {
        //this.game.state.start('game')
    }

    createDimOverlay () {
        
    }

    reorientLayout (orientation) {
        
    }
}
