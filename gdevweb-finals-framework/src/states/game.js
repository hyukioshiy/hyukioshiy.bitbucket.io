class Game extends Phaser.State {
    constructor() {
        super();
        
        this.config = {
            landscape: {
                background: {
                    x: game.baseResolution.max * 0.5,
                    y: game.baseResolution.min * 0.5, 
                    angle: 90
                },
                installButton: {
                    x: game.baseResolution.max * 0.5,
                    y: game.baseResolution.min * 0.1,
                    scaleX: 1,
                    scaleY: 1.2
                },
                endCard: {
                    x: game.baseResolution.max * 0.5,
                    y: game.baseResolution.min * 0.5,
                    scaleX: 1,
                    scaleY: 1.2
                }
            },
            portrait: {
                background: {
                    x: game.baseResolution.min * 0.5,
                    y: game.baseResolution.max * 0.5,
                    angle: 0
                },
                installButton: {
                    x: game.baseResolution.min * 0.5,
                    y: game.baseResolution.max * 0.05,
                    scaleX: 1,
                    scaleY: 1
                },
                endCard: {
                    x: game.baseResolution.min * 0.5,
                    y: game.baseResolution.max * 0.5,
                    scaleX: 1,
                    scaleY: 1
                }
            }
        }
    }

    preload() {
        
    }

    create() {
        // game container (for resizing)
        game.container = game.world.addChild (new Phaser.Group (game));
        game.container.reorientLayout = this.reorientLayout.bind(this);

        // game container
        this.container = new Phaser.Group (game);
        game.container.add (this.container);

        // background
        this.background = this.container.add (this.game.add.graphics());
        this.background.beginFill (0x333333);
        this.background.drawRect (-game.baseResolution.min*0.5, -game.baseResolution.max*0.5, game.baseResolution.min, game.baseResolution.max);
        this.background.endFill ();
        this.background.anchor.setTo (0.5, 0.5);

        // install button at top
        this.installButton = new InstallButton (game);
        this.container.addChild (this.installButton);

        this.endCard = new EndCard (game);
        
        // update orientation
        adaptGameToOrientation();
        assignOrientationChangeHandlers();

        this.gameStart ();
    }

    gameStart () {
        // add a delay timer that will trigger game over
        game.time.events.add(Phaser.Timer.SECOND * 5, function () {
            this.gameOver ("win");
        }, this);
    }

    gameOver (condition) {
        // hide the install button at the top
        this.installButton.visible = false;

        // create a transparent black background to dim the game
        this.createDimOverlay ();

        // add the end card on top of the game
        this.container.addChild (this.endCard);

        // pass in the condition to the end card to modify the header text
        this.endCard.show (condition);
    }

    createDimOverlay () {
        this.overlay = this.container.add (this.game.add.graphics());
        this.overlay.beginFill (0x000000, 0.5);
        this.overlay.drawRect (-game.baseResolution.max*0, -game.baseResolution.max*0, game.baseResolution.max, game.baseResolution.max);
        this.overlay.endFill ();
    }

    reorientLayout (orientation) {
        this.orient = orientation;

        this.background.angle = this.config[orientation].background.angle;
        this.background.x = this.config[orientation].background.x;
        this.background.y = this.config[orientation].background.y;

        this.installButton.x = this.config[orientation].installButton.x;
        this.installButton.y = this.config[orientation].installButton.y;
        this.installButton.scale.x = this.config[orientation].installButton.scaleX;
        this.installButton.scale.y = this.config[orientation].installButton.scaleY;

        this.endCard.x = this.config[orientation].endCard.x;
        this.endCard.y = this.config[orientation].endCard.y;
        this.endCard.scale.x = this.config[orientation].endCard.scaleX;
        this.endCard.scale.y = this.config[orientation].endCard.scaleY;
    }
}
