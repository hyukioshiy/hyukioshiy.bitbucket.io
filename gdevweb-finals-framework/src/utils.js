const checkLanguage = () => {
    // check browser language
    if (navigator.language.indexOf('en') > -1) {
        return 'en';
    } else {
        return navigator.language;
    }
}

const isMobile = {
    Android: function() {
        return navigator.userAgent.match(/Android/i);
    },
    BlackBerry: function() {
        return navigator.userAgent.match(/BlackBerry/i);
    },
    iOS: function() {
        return navigator.userAgent.match(/iPhone|iPad|iPod/i);
    },
    Opera: function() {
        return navigator.userAgent.match(/Opera Mini/i);
    },
    Windows: function() {
        return navigator.userAgent.match(/IEMobile/i) || navigator.userAgent.match(/WPDesktop/i);
    },
    any: function() {
        return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
    }
}

function getViewportDimensions() {
    game.viewportDimensions.width = window.gamebox.clientWidth || window.document.documentElement.clientWidth;
    game.viewportDimensions.height = window.gamebox.clientHeight || window.document.documentElement.clientHeight;
}

function resizeGameBox(width, height) {
    gamebox.style.width = (width || document.documentElement.clientWidth) + "px";
    gamebox.style.height = (height || document.documentElement.clientHeight) + "px";
}

function resizeCanvas() {
    var width = game.viewportDimensions.width;
    var height = game.viewportDimensions.height;
    
    game.canvas.style.height = height + "px";
    game.canvas.style.width = width + "px";

    game.width = game.stage.width = width;
    game.height = game.stage.height = height;

    game.isLandscape = game.height < game.width;
    
    game.renderer.resize(width, height);

    game.input.scale.x = 1;
    game.input.scale.y = 1;

    game.worldScale = calculateGlobalScale();
}

function calculateGlobalScale() {
    var globalScale = {};
    

    if (game.isLandscape) {
        globalScale.x = game.width / game.baseResolution.max;
        globalScale.y = game.height / game.baseResolution.min;
        globalScale.width = game.baseResolution.max;
        globalScale.height = game.baseResolution.min;
    } else {
        globalScale.x = game.width / game.baseResolution.min;
        globalScale.y = game.height / game.baseResolution.max;
        globalScale.width = game.baseResolution.min;
        globalScale.height = game.baseResolution.max;
    }

    return globalScale;
}

function resizeGame(width, height) {
    if(width && height) {
        resizeGameBox(width, height);
    }

    getViewportDimensions();

    if(!needsResize()) {
        return;
    }

    resizeCanvas();
    adaptGameToOrientation();
}

function needsResize() {
    return game.width !== game.viewportDimensions.width || game.height !== game.viewportDimensions.height;
}

function adaptGameToOrientation() {
    if (game.isLandscape) {
        game.worldOffset = {
            x: 25 * game.worldScale.x,
            y: -35 * game.worldScale.y
        };

        refreshWorldScale();

        game.container.reorientLayout("landscape");
    } else {
        game.worldOffset = {
            x: 0,
            y: 0
        };

        refreshWorldScale();

        game.container.reorientLayout("portrait");
    }
}

function refreshWorldScale() {
    if (game.container) {
        game.container.scale.setTo(game.worldScale.x, game.worldScale.y);
    }
}

function assignOrientationChangeHandlers() {
    if (game.resizeInterval == null) {
        window.addEventListener('resize', resizeGameBox);
        game.resizeInterval = setInterval(function() {
            resizeGame();
        }, 100);
    }
}